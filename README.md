<h1>How to launch the tests</h1>
<h2>Locally</h2>

<h4>Mac OS</h4>

use the command: ``` /Applications/Unity/Unity.app/Contents/MacOS/Unity -runTests -batchmode -projectPath PATH_TO_YOUR_PROJECT -testResults PATH_TO_YOUR_PROJECT_RESULTS_DIRECTORY/results.xml -testPlatform PlayMode ```

<h4>Windows</h4>

use the command: ``` PATH_TO_UNITY_DIRECTORY\Unity.exe -runTests -batchmode -projectPath PATH_TO_YOUR_PROJECT -testResults PATH_TO_YOUR_PROJECT_RESULTS_DIRECTORY/results.xml -testPlatform PlayMode ```

<h1>Unity.AI package</h1>
<h2>NavMeshAgent{}</h2>

AgentMoveWithZeroSpeedTest
new NavMeshAgent created
set Speed as 0
Set Destination as correct Vector3 value which differs from the start position
all the other params by default
Start moving
Expected: NavMeshAgent object still on the same coordinate, as cannot move anywhere with 0 speed.
Actual: NavMeshAgent.position is the same as before the start.

AgentAccelerationEqualZeroTest
new NavMeshAgent created
set Speed as 1
set Acceleration as 0
Set Destination as correct Vector3 value which differs from the start position
all the other params by default
Start moving
Expected: NavMeshAgent object still on the same coordinate, as cannot move anywhere with 0 acceleration.
Actual: NavMeshAgent.position is the same as before the start.

AgentDestinationSpecifiedBy2Coordinates
new NavMeshAgent created
set Speed as 3
set Acceleration as 1
set initial agent coordinates as (5,0,1)
set Destination as correct Vector2 value(x,y)/(x,z)/(y,z) = (7, 0)
all the other params by default
start Moving
wait until the moving is ended
Expected: NavMeshAgent object stoped in the coordinate (7,0,1).
Actual: actual NavMeshAgent.position is (7, 0.08333331, 0.6666667)
 
AgentHighSpeedMoveUsingAutoBrakingTest
new NavMeshAgent created
set Speed as 20
set Acceleration as 1
set Destination as correct Vector3 value which differs from the start position
all the other params by default
start moving
wait until the moving is ended
Expected: NavMeshAgent object spent half less time to stop at the destination coordinate with auto braking than without it.
Actual: NavMeshAgent object spent 87sec instead of 152sec without using auto braking.

AgentMoveWithoutNavMeshLoadedTest
new NavMeshAgent created
Set Destination as correct Vector3 value which differs from the start position
all the other params by default
NavMesh isn’t baked
start moving
Expected: NavMeshAgent didn’t start moving
Actual: NavMesh object isn’t built, error exception is thrown during compilation, NavMeshAgent cannot operate without NavMesh.





Conclusion: Unity engine world has its geometry system and physics, with certain rules for objects and variables. Some of the objects are quite closely related to each other and cannot work separately. So in some cases, it’s even hard to predict the behaviour of an object, until you will know the right one.
Unity built-in compiler has flexible rules for certain cases where the default .net compiler would work more strictly.
I’m pretty interested in what exact algorithms are used for object movement calculations because from time to time it works quite unexpectedly. An object could ignore a straight trajectory to its destination and slide near edges of allowed navMesh.
In the end, I could add that for quite a long time I haven’t been so interested in something to spend about 10hours in a row with, like with unity engine, huge knowledge base and just a fun sandbox.
Regarding autotests, unfortunately, I spend much more time on research and education than I expected so have nothing to boast about. Sure there are a lot of things that could be improved, a few additional layers of logic, some wrappers, of course, report tools something like Allure for ex., but the deal is deal, Thursday is the deadline. As you will understand, the main pain in my head was that NavMeshBuilder, after tons of articles and videos, didn't find a way to use it properly out of the editor. Some of the navMeshAgent object functions in run time have a bit different behaviour in comparison with the editor mode run.



Vladislav Lesnevsky
UnityEngine 2021.2.7f1, MacOS Catalina
