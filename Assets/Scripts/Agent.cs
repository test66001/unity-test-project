using UnityEngine;
using UnityEngine.AI;
    
public class Agent : MonoBehaviour
{

    public Transform goal;

    private NavMeshAgent player;


    private void Start()
    {
        UnityEditor.AI.NavMeshBuilder.BuildNavMesh();
        player = GetComponent<NavMeshAgent>();
        player.destination = goal.position;
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SetDestinationToMousePosition();
        }
    }
        

    private void SetDestinationToMousePosition()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            player.SetDestination(hit.point);
        }
    }

}