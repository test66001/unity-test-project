using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.AI;


[TestFixture]
public class AgentTests
{
    private Transform target;
    private NavMeshAgent agent;
    private NavMeshSurface surface;


    [SetUp]                                           
    public void Setup()
    {
        target = null;
        agent = null;
        surface = null;

        // arrange
        surface = GameObject.CreatePrimitive(PrimitiveType.Plane)
            .AddComponent<NavMeshSurface>();

        var targetPrefab = Resources.Load("Tests/Finish");

        target = ((GameObject)targetPrefab).transform;

        surface.BuildNavMesh();

        agent = new GameObject("Agent").AddComponent<NavMeshAgent>();

        target.position = new Vector3(5f, 0f, 0f);
    }

    [UnityTest] 
    public IEnumerator AgentConnectedToNavMeshTest()
    {
        surface.BuildNavMesh();

        yield return null;

        Assert.AreEqual(surface, agent.navMeshOwner);
        Assert.IsTrue(agent.isOnNavMesh);
    }

    [UnityTest]
    public IEnumerator AgentMoveWithZeroSpeedTest()
    {
        // act
        var initialPosition = agent.nextPosition;

        agent.speed = 0;
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(1);

        // assert
        Assert.AreEqual(agent.nextPosition, initialPosition);
    }


    [UnityTest]
    public IEnumerator AgentMoveLowSpeedTest()
    {
        // act
        var initialPosition = agent.nextPosition;

        agent.speed = -float.MinValue;
        agent.acceleration = int.MaxValue;

        agent.SetDestination(target.position);

        yield return new WaitForSeconds(1);

        // assert
        Assert.AreNotEqual(agent.nextPosition, initialPosition);
    }


    [UnityTest]
    public IEnumerator AgentMoveHighSpeedTest()
    {
        // act
        var initialPosition = agent.nextPosition;

        agent.speed = int.MaxValue;
        agent.acceleration = 1;
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(1);

        // assert
        Assert.AreNotEqual(agent.nextPosition, initialPosition);
    }


    [UnityTest]
    public IEnumerator AgentAccelerationEqualZeroMoveTest()
    {
        // act
        var initialPosition = agent.nextPosition;

        agent.acceleration = 0;
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(1);

        // assert
        Assert.AreEqual(agent.nextPosition, initialPosition);
    }


    [UnityTest]
    public IEnumerator AgentDestinationCorrectTest()
    {
        // act
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(10);

        // assert
        Assert.AreEqual(agent.nextPosition, target.position);
    }


    [UnityTest]
    public IEnumerator AgentDestinationSpecifiedByVector2Test()
    {
        // act
        var target = new Vector2(7, 0);

        agent.acceleration = int.MaxValue;
        agent.SetDestination(target);

        yield return new WaitForSeconds(5);

        // assert
        Assert.AreNotEqual(agent.nextPosition, target);
    }


    [UnityTest]
    public IEnumerator AgentHighSpeedMoveWithoutAutoBrakeTest()
    {
        // act
        agent.speed = 30;
        agent.acceleration = 10;
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(10);

        // assert
        Assert.AreNotEqual(agent.nextPosition, target.position);
    }


    [UnityTest]
    public IEnumerator AgentHighSpeedMoveWithAutoBrakeTest()
    {
        // act
        agent.speed = 30;
        agent.acceleration = 10;
        agent.autoBraking = true;
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(10);

        // assert
        Assert.AreEqual(agent.nextPosition, target.position);
    }

    [UnityTest]
    public IEnumerator AgentStoppingDistanceTest()
    {
        // act
        agent.stoppingDistance = 2f;
        agent.SetDestination(target.position);

        yield return new WaitForSeconds(5);

        var dist = Vector3.Distance(target.position, agent.nextPosition);

        // assert
        Assert.True(dist < agent.stoppingDistance);
    }

    [UnityTest]
    public IEnumerator AgentMoveWithoutNavMeshLoadedTest()
    {
        // act
        try
        {
            var initialPosition = agent.pathEndPosition;

            agent.SetDestination(target.position);

            surface.RemoveData();
        }
        catch (System.Exception)
        {
            // assert
            Assert.Fail();
        }

        yield return null;
    }


    [TearDown]
    public void TearDown()
    {
        UnityEditor.AI.NavMeshBuilder.ClearAllNavMeshes();

        Object.DestroyImmediate(agent.gameObject);
        Object.DestroyImmediate(surface.gameObject);
    }
}